<?php

namespace azbuco\mdi;

use yii\web\AssetBundle;

class MdiAsset extends AssetBundle
{
    public $sourcePath = '@bower/mdi';
    public $css = [
        'css/materialdesignicons.min.css',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
