<?php

namespace azbuco\mdi;

use yii\bootstrap\Widget;
use yii\helpers\Html;

class MdiWidget extends Widget
{
    public $icon = 'alert';

    /**
     * @inheritdoc
     */
    public function init()
    {
        Html::addCssClass($this->options, 'mdi');
        Html::addCssClass($this->options, 'mdi-' . $this->icon);
        
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerBundle();
        return Html::tag('i', '', $this->options);
    }

    /**
     * Registers plugin and the related events
     */
    protected function registerBundle()
    {
        $view = $this->getView();
        MdiAsset::register($view);
    }
}
