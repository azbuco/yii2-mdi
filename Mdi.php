<?php

namespace azbuco\mdi;

class Mdi
{

    public static function icon($icon, $class = '')
    {
        $config = [
            'icon' => $icon,
        ];
        if ($class) {
            $config['options'] = ['class' => $class];
        }
        return MdiWidget::widget($config);
    }

}
